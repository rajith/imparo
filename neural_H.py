import keras
import time
import numpy as np

from keras.layers import Dense, LSTM, SimpleRNN, GRU
from keras.models import Sequential, load_model

from dataset import DataSet

def create_model_1(timesteps):
    '''
    Create the neural network 1
    Has 1 layer of LSTM
    :param timesteps:
    :return:
    '''
    model = Sequential()
    model.add(LSTM(32, input_shape=(timesteps, 5)))
    model.add(Dense(1))
    model.compile(loss = 'mse',
                  optimizer = 'rmsprop')
    return model

def create_model_2(timesteps):
    '''
    Create the neural network 2
    Has 1 layer of SimpleRNN
    :param timesteps:
    :return:
    '''
    model = Sequential()
    model.add(SimpleRNN(32, input_shape=(timesteps, 5)))
    model.add(Dense(1))
    model.compile(loss = 'mse',
                  optimizer = 'rmsprop')
    return model

def create_model_3(timesteps):
    '''
    Create the neural network 3
    Has 1 layer of Gated Recurrent Units
    :param timesteps:
    :return:
    '''
    model = Sequential()
    model.add(GRU(32, input_shape=(timesteps, 5)))
    model.add(Dense(1))
    model.compile(loss = 'mse',
                  optimizer = 'rmsprop')
    return model

def train_model(model, file_name, timesteps):

    print("Training model: "+file_name)

    ds = DataSet(n=100, alpha=9, s=1, time=timesteps, p=0.01)
    ds.run()

    x_train, y_train = transform_data_for_testing(ds)

    hist = model.fit(x_train, y_train, batch_size=128, nb_epoch=10,
                     validation_split=0.1, verbose=0)

    model.save(file_name)
    print("FInished training model, and saved to: "+file_name)


def load_model_from_file(path):
    '''
    Load a neural network model from a file location
    :param path: location of the saved model
    :return: the model
    '''
    return load_model(path)

def transform_data_for_testing(dataset):
    '''
    Transforms a dataset for testing / or training the Neural Network to find H
    :param dataset: the dataset to be used
    :return: x_test, y_test vectors
    '''
    #print dataset.n
    x_test = np.empty([dataset.n * (dataset.n-1), dataset.time, 5])
    y_test = np.empty([dataset.n * (dataset.n-1)])

    counter = 0
    for i in range(dataset.n):
        counter2 = 0
        for j in range(dataset.n):
            if i == j:
                continue
            if ( dataset.G[i, j] ):
                x_test[counter, :, counter2] = dataset.x[i]
                counter2 += 1
                if(counter2 == 5):
                    break
        y_test[counter] = dataset.H[i, i]
        counter += 1

    return x_test, y_test

def run_tests(num_test, model, timesteps):
    '''
    Runs num_test number of tests on the model
    :param num_test: number of tests to be run
    :param model: the model to be run
    :return: None. Output is printed to the screen
    '''

    print("Starting tests")

    sum = 0.0

    ds1 = DataSet(n=10, alpha=9, s=1, time=timesteps, p=0.1)

    G_global = ds1.G;

    for i in range(num_test):

        ds = DataSet(n=10, alpha=9, s=1, time=timesteps, p=0.1, G=G_global)
        ds.run()
        x_test, y_test = transform_data_for_testing(ds)

        score, acc = model.evaluate(x_test, y_test, verbose=0, batch_size=16)
        #print(acc)
        sum += acc

    print("Finished tests")
    print("Average accuracy: "+str(sum/num_test))

class TimeHistory(keras.callbacks.Callback):
    '''
    Callbacks for timing the neural network epochs
    '''
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        print time.time() - self.epoch_time_start
        self.times.append(time.time() - self.epoch_time_start)
