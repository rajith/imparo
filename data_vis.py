from dataset import DataSet


timesteps = 100

# Model will be trained using this dataset

for i in range(1):
    ds = DataSet(n=2, alpha=5, s=1, time=timesteps, p=0.5)
    ds.run()
