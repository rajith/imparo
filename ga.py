import numpy as np
from tools import GLV_with_percentage
from tools import BCD
from tools import draw_network
from data_preprocessing import Dataset
from datasim import init_sim
from datasim import get_r

from sys import exit
import matplotlib.pyplot as plt
from copy import deepcopy
import os
import datetime
import pandas as pd
import networkx as nx
import sys
from sklearn.metrics import mean_squared_error
from scipy.stats import ks_2samp


DEBUG = True


def log(*string):
    global DEBUG
    if DEBUG:
        print(list(string))


class Chromosome:

    # Chromosome is a list of genes
    def __init__(self, n, t):
        self.genes = []
        self.fitness = 0.0
        self.score = 0.0
        self.N = n
        self.A = np.zeros((n, n))
        self.T = t
        self.r = get_r(n)

    def add_gene(self, gene):
        self.genes.append(gene)

    def calculate_score(self):

        # REMOVE

        # self.A = np.zeros((self.N, self.N))

        for gene in self.genes:
            self.A[gene.i, gene.j] = gene.A

        # TODO : check whether setting diagonal to -1 improves
        #for i in range(self.N):
        #    self.A[i, i] = -0.5

        global XC1
        global K

        X = GLV_with_percentage(XC1, self.A, self.T, self.N, self.r)
        bcd = BCD(X, K, self.T, self.N)

        # TODO : add KS Statistic
        global KS_A
        ks_stat = ks_2samp(self.A.flatten(), KS_A)

        self.score = (1 - bcd - ks_stat[0]*0)
        if np.isnan(self.score):
            self.score = 0
        # log(self.score)

    def set_fitness(self, fitness):
        self.fitness = fitness

    def mutate(self, chance):

        for i in range(len(self.r)):
            rand = np.random.rand()
            if rand < chance:
                # do the randomization
                self.r[i] += np.random.normal(0, 0.01)

        for gene in self.genes:
            gene.mutate(chance)

    def visualize(self):
        # TODO improve visualization
        plt.figure(figsize=(fig_x, fig_y))
        plt.imshow(self.A, cmap='hot', interpolation='nearest')
        # plt.show()
        plt.savefig(folder_path+"interactions.svg")

    def print_a(self):
        np.set_printoptions(precision=3)
        print(self.A)
        print(np.count_nonzero(self.A))

    def draw_netowrk_graph(self):
        global ls
        plt.figure(figsize=(fig_x, fig_y))
        G1 = nx.MultiDiGraph()

        matrix = self.A
        rows = matrix.shape[0]
        cols = matrix.shape[1]

        for x in range(0, rows):
            G1.add_node(x, name=ls[x])

        for x in range(0, rows):
            for y in range(0, cols):
                if x != y:
                    if(matrix[x][y] > 0):
                        G1.add_edge(x, y, weight=pow(matrix[x][y], 2), color="blue")
                    else:
                        G1.add_edge(x, y, weight=pow(matrix[x][y] * -1, 2), color="red")

        pos = nx.shell_layout(G1)
        ax = plt.gca()
        draw_network(G1, pos, ax)
        ax.autoscale()
        # plt.show()
        plt.savefig(folder_path + "interactions_graph.svg")

class Gene:

    # A gene is a tuple: i, j and A_i,j
    # N is the total number of OTUs

    def __init__(self, i, j, a, n):
        self.i = i
        self.j = j
        self.A = a

        self.N = n

    def mutate(self, chance):

        # mutate i
        r = np.random.rand()
        if r < chance:
            # do the randomization
            np.random.randint(0, self.N)

        # mutate j
        r = np.random.rand()
        if r < chance:
            # do the randomization
            np.random.randint(0, self.N)

        # mutate A
        r = np.random.rand()
        if r < chance:
            # TODO parametrize sigma
            self.A += np.random.normal(0, 0.01)


def initpoprandom(t, n, pop_size=10000, target_connections=0.1):
    population = []

    gene_size = int(n * n * target_connections)

    # log("InitPopRandom", T, N)
    # TODO Initialize the population with A = (NH)oG type of dataset

    # for x in range(pop_size):

        # Initpop_A, temp1, temp2 = init_sim(n=n, time=300, pr=False, ER=True, sigma=0.1, alpha=0.2, p=target_connections)
        # chrom = Chromosome(n, t)
        # for i in range(n):
        #     for j in range(n):
        #         if Initpop_A[i][j] != 0:
        #             _I = i
        #             _J = j
        #             A_ij = Initpop_A[i][j]
        #
        #             gene = Gene(_I, _J, A_ij, n)
        #             chrom.add_gene(gene)
        # r = get_r(n)
        # chrom.r = r
        # population.append(chrom)
        # print(len(chrom.genes), "asdf")

    for i in range(pop_size):
        chrom = Chromosome(n, t)
        for j in range(gene_size):
            _I = np.random.randint(0, n)
            _J = np.random.randint(0, n)
            # TODO parametrize sigma
            A_ij = np.random.normal(0, 0.5)

            gene = Gene(_I, _J, A_ij, n)
            chrom.add_gene(gene)
        r = get_r(n)
        chrom.r = r
        population.append(chrom)
    #     print(len(chrom.genes), "asdfasdf")
    #


    return population


def printpopulation(population):
    global DEBUG
    if DEBUG:

        for chrom in population:

            for gene in chrom.genes:
                print(gene.i, gene.j, gene.A)

            log("End Chromosome")

        log("End Population")


def visualize_x(x):
    plt.figure(figsize=(fig_x, fig_y))

    for i in range(x.shape[0]):
        plt.plot(x[i, :])
    plt.legend(bbox_to_anchor=(1.05, 1))
    # plt.show()


def visualize_x_stack(x, figname="X"):
    global xs
    global ls

    ys = x.tolist()

    plt.figure(figsize=(fig_x, fig_y))

    plt.stackplot(xs, *ys, labels=ls)

    plt.legend(bbox_to_anchor=(1.05, 1))
    # plt.show()
    plt.savefig(folder_path+figname+".svg")


def calculate_fitness(pop):
    sum_score = 0.0

    for chrom in pop:
        sum_score += chrom.score

    for chrom in pop:
        chrom.set_fitness(chrom.score / sum_score)


def pick_random(pop):
    # type: (list) -> Chromosome
    tries = 0

    while tries < 5000:
        random_int = np.random.randint(0, len(pop))

        monte = np.random.rand()

        if pop[random_int].fitness > monte:
            return pop[random_int]
        tries += 1

    return pop[np.random.randint(0, len(pop))]


def crossover(chrom1, chrom2):
    # corssover point
    cp = np.random.randint(0, min(len(chrom1.genes), len(chrom2.genes)))

    # print len(chrom1.genes), len(chrom2.genes), cp

    #TODO : crossover r

    for i in range(cp):
        chrom1.genes[i] = deepcopy(chrom2.genes[i])

    cp2 = np.random.randint(0, chrom1.N)

    for i in range(cp2):
        chrom1.r[i] = chrom2.r[i]

    return chrom1


def run_ga(dataset, pop_size=100, mutate_chance=0.01, num_gen=1000, target_connections=0.1, print_gen=100):
    GA_Stats = []

    # pop: - current population
    # gen: no of generations
    # best_fitness: best fitness so far
    # pop_size: population size

    N = dataset.get_N()
    T = dataset.get_time()

    log("run_GA", T, N)

    # Initialize with initial population
    pop = initpoprandom(T, N, pop_size, target_connections)
    gen = 0

    # printPopulation(pop)

    global K
    visualize_x_stack(K, figname="Original")
    #TODO Figure ^ out

    best_score = 0.0
    best_chrom = None
    best_X = None

    while gen < num_gen:

        for chrom in pop:
            chrom.calculate_score()

        # pop.sort(key=lambda x: x.score, reverse=True)
        best_chrom = max(pop, key=lambda x: x.score)
        GA_Stats.append(best_chrom.score)

        global XC1
        if gen % print_gen == 0:
            log("Gen: ", gen, best_chrom.score, (best_chrom.score - best_score) / best_chrom.score)
            best_score = best_chrom.score
            # best_chrom.print_a()

        # Last generation examination
        #if gen == num_gen-1:
        #    log("Last Gen: ", gen, len(pop))
        #    #pop = pop.sort(key=lambda x: x.score)
        #    for chrom in pop:
        #        log(chrom.score)


        best_X = GLV_with_percentage(XC1, best_chrom.A, T, N, best_chrom.r)

        calculate_fitness(pop)

        new_pop = [best_chrom]

        for i in range(0, pop_size / 2 - 1):
            parent1 = deepcopy(pick_random(pop))
            parent2 = pick_random(pop)

            child = deepcopy(pick_random(pop))
            child = crossover(parent1, parent2)
            child.mutate(mutate_chance)

            new_pop.append(child)

        alt_pop = initpoprandom(T, N, pop_size - ((pop_size / 2) - 1), target_connections)
        new_pop.extend(alt_pop)

        pop = new_pop
        gen += 1

    print("Best:", best_chrom.score)
    visualize_x_stack(best_X, figname="Best")
    return GA_Stats, best_chrom


# log(sys.argv)
# From sys arguments
# 1 filepath
# 2 simulated 1 or 0
# 3 no of OTUs
# 4 population size
# 5 number of generations
# 6 print each n generation : n

time_str = datetime.datetime.now().strftime("%y%m%d-%H%M%S")
folder_path = "Results"+time_str
os.makedirs(folder_path)
folder_path += "/"

log(folder_path)

#file = "SimData/190131-181103_AP"
#file = "SimData/190131-125839_AP"
file = sys.argv[1]

#file = "SimData/1"
K_ds = Dataset.from_csv(str(file))
log(file)

# in order to view the dataset as a dataframe
DF = K_ds.df

f = open(folder_path+"info.txt", "w+")

fig_x = 20
fig_y = 10
simulated = bool(int(sys.argv[2]))
K_ds.preprocess(is_simulated=simulated)

# in order to view the dataset as a dataframe
DF2 = K_ds.df

n_OTU = int(sys.argv[3])
K2 = K_ds.get_subset_by_rank(n_OTU, largest=True, random=False)




K = K2.matrix()
XC1 = K2.matrix()[:, 0]
xs = map(int, list(K2.df))
ls = list(K2.df.index)


pop_size = 10
if len(sys.argv) > 4:
    pop_size = int(sys.argv[4])

num_gen = 5000
if len(sys.argv) > 5:
    num_gen = int(sys.argv[5])

print_gen = 50
if len(sys.argv) > 6:
    print_gen = int(sys.argv[6])

target_connections = 0.5
if len(sys.argv) > 7:
    target_connections = float(sys.argv[7])


mutate_chance = 0.02
if len(sys.argv) > 8:
    mutate_chance = float(sys.argv[8])


# create a sample dataset for KS-Statistic Generation
global KS_A
KS_A, temp1, temp2 = init_sim(n=pop_size, time=300, pr=True, ER=True, sigma=0.1, alpha=0.2, p=target_connections)
KS_A = KS_A.flatten()

print(folder_path)

stats, best = run_ga(K2, pop_size=pop_size, mutate_chance=mutate_chance, num_gen=num_gen,
                     target_connections=target_connections, print_gen=print_gen)
f.write("File"+str(file)+" , pop_size="+str(pop_size)+" , mutate_chance="+str(mutate_chance)+" , num_gen="+str(num_gen)+
        " , target_connections="+str(target_connections))

f.write("\n")
f.write(str(best.score))
# print(stats)

plt.figure(figsize=(fig_x, fig_y))
plt.plot(stats)
# plt.show()
plt.savefig(folder_path+"training.svg")

best.visualize()

df_best_A = pd.DataFrame(best.A)
df_best_A.to_csv(folder_path+"MIN_"+time_str+".csv")

best.draw_netowrk_graph()

#if simulated compare the answers with the ground truth
if(simulated):
    # import the ground truth matrix
    ground_truth = pd.read_csv(file[:-4]+"_MIN.csv").to_numpy()
    ground_truth = ground_truth[:, 1:]
    # get the infered matrix
    inferred = df_best_A.to_numpy()

    # flatten arrays
    ground_truth_flat = ground_truth.flatten()
    inferred_flat = inferred.flatten()

    # calculate MSE and NMSE
    MSE = mean_squared_error(ground_truth_flat, inferred_flat)
    log("MSE",str(MSE))
    f.write("\n")
    f.write(str(MSE))

    # draw graphs

    X = np.linspace(np.amin(ground_truth_flat), np.amax(ground_truth_flat))
    Y = X

    plt.figure(figsize=(3, 3))
    plt.rcParams.update({'font.size': 5})
    plt.scatter(ground_truth_flat, inferred_flat, marker='o', facecolors='none', edgecolors='C0')
    plt.plot(X, Y)
    plt.xlabel("Truth")
    plt.ylabel("Estimation")
    # plt.show()
    plt.savefig(folder_path + "Truth-Estimation.svg")

f.close()
exit(0)
