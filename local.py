import neural_G as nnG
import neural_H as nnH
import sys
import keras
import time
import numpy as np

from keras.layers import Dense, LSTM, SimpleRNN, GRU
from keras.models import Sequential, load_model

from dataset import DataSet



timesteps = 100

# Model will be trained using this dataset
ds = DataSet(n=10, alpha=1, s=1, time=timesteps, p=0.5)
ds.run()
ds.visualize()
ds.visualize_G()
# ds.visualize_H() # To be implemented

modelG = nnG.create_model_3(timesteps)
modelH = nnH.create_model_1(timesteps)

nnG.train_model(modelG, "model_G_local.h5", timesteps)

nnG.run_tests(10, modelG, timesteps)

# Model will be tested (to predict G) using this dataset

ds_test = DataSet(n=10, alpha=1, s=1, time=timesteps, p=0.5)
ds_test.run()
#ds_test.visualize(filename="test_vis.html")
#ds_test.visualize_G(filename="test_vis_G.html")

G = ds_test.G
G_cross = nnG.predict_G(ds_test, modelG)

ds_test.visualize_G(filename="G_cross.html", G_=G_cross)


accuracy = nnG.compare_G(G, G_cross)

print("Accuracy: ", accuracy)











