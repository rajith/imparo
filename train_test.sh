#!/bin/bash
#PBS -P ep84
#PBS -q normalbw 
#PBS -l ncpus=28
#PBS -l mem=128GB
#PBS -l jobfs=400GB
#PBS -l walltime=10:00:00
#PBS -l software=python
#PBS -l wd
 
# Load modules.
module load python/2.7.11
module load tensorflow/1.2.1-python2.7

# keras
export PYTHONPATH=$PYTHONPATH:$HOME/keras
 
# Run Python applications
python train.py ${nettype} ${timesteps} ${numtests} ${name}.h5 > logs/${name}.log 2> logs/${name}.err
