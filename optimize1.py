from dataset import DataSet
from tools import GLV
from tools import BCD
import numpy as np
import pandas as pd
import time

timesteps = 100
N = 100

# Create an A (interaction matrix)
ds = DataSet(n=N, alpha=2, s=1, time=timesteps, p=0.5)

# Display it
ds.visualize_A(filename="vis1 - A.html")

# Populate it with GLV
XC1 = np.random.uniform(0, 1, N)
X = GLV(XC1, ds.A, timesteps, N)

# Display it
ds.visualize(x=X, x_axis_len=timesteps, filename="vis1 - X.html")

# Also write into a CSV - pop1.csv
#print(X)
df = pd.DataFrame(data = X)
df.to_csv("X.csv")

# add noise to A
noise = np.random.normal(0, 0.5, N*N)
#print(noise)
noise_3D = np.reshape(noise, (N, N))
#print(noise_3D)

new_A = ds.A + noise_3D


# Display it
#ds.visualize_A(A=new_A, filename="vis2 - A.html")

# Populate new one with GLV
#X2 = GLV(XC1, new_A, timesteps, N)

# Display it
#ds.visualize(x=X2, x_axis_len=timesteps, filename="vis2 - X.html")

# Write to a CSV - pop2.csv
##df.to_csv("X2.csv")

# Compare the two with BCD
#print("BCD")
#print(BCD(X, X2, timesteps, N))

# timing the GLV and BCD

t = time.clock()
t1 = time.time()
for i in range(1000):
    X2 = GLV(XC1, new_A, timesteps, N)
    BCD = (X, X2, timesteps, N)
elapsed_time = time.clock() - t
elapsed_wall_time = time.time() - t1
print elapsed_time/1000, elapsed_wall_time/1000