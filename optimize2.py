from dataset import DataSet
from tools import GLV
from tools import BCD
from tools import InitPop_Random
from tools import printPopulation
import numpy as np
import pandas as pd
import time

# Important! Set accordingly
timesteps = 100
N = 100

t = time.clock()
for i in range(1000):
    population = InitPop_Random(N, pop_size=10000)
t = (time.clock() - t)/1000.0
print(t)

# import data from CSV into pandas


# get pandas data into 'K' a np matrix

# visualize it

# Create an A (interaction matrix) ---- how do we create the initial matrix????
# For now Random(?)
#ds = DataSet(n=10, alpha=2, s=1, time=timesteps, p=0.5)

# Display it
#ds.visualize_A(filename="vis1 - A.html")

# Populate it with GLV
#XC1 = np.random.uniform(0, 1, N)
#X = GLV(XC1, ds.A, timesteps, N)

# Display it
#ds.visualize(x=X, x_axis_len=timesteps, filename="vis1 - X.html")

# Also write into a CSV - pop1.csv
#print(X)
#df = pd.DataFrame(data = X)
#df.to_csv("X.csv")

# Compare the two with BCD
#print("BCD")
#print(BCD(K, X, timesteps, N))