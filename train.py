import neural_G as nnG
import sys

# Takes following arguments

# arg 1: Model Type 1. LSTM, 2. RNN, 3. GRU
# arg 2: TimeSteps
# arg 3: No of tests to run
# arg 4: File Name for the model


timesteps = int(sys.argv[2])
#arg2 : timesteps

if(sys.argv[1] == 1):
    #arg 1 = 1: LSTM model
    model = nnG.create_model_1(timesteps)

elif(sys.argv[1] == 2):
    #arg 1 = 2: RNN model
    model = nnG.create_model_2(timesteps)

elif(sys.argv[1] == 3):
    #arg 1 = 3: GRU model
    model = nnG.create_model_3(timesteps)

else:
    model = nnG.create_model_1(timesteps)


nnG.train_model(model, sys.argv[4], timesteps)
#arg 4: Filename for the model to be saved


nnG.run_tests(int(sys.argv[3]), model, timesteps)
# arg 3 : how many tests to run
exit()
