# import statements

import math
import numpy as np
import networkx as nx

import plotly
import plotly.graph_objs as go
import scipy.signal as sg

import matplotlib.pyplot as plt


# class
class DataSet:

    def __init__(self, n=5, alpha=1.1, s=1, time=300, p=1, G = None, N = None, H = None):
        # n - number of OTUs
        self.n = n
        # alpha - Coefficient for PowerLaw Distribution
        self.alpha = alpha
        # s - scale
        self.s = s
        # time - no of time points to generate
        self.time = time
        # p - Coefficient for Erdos-Renyi Network / By default all in
        self.p = p

        if N is None:
            self.N = self.init_N()
        else:
            self.N = N
        if H is None:
            self.H = self.init_H()
        else:
            self.H = H
        if G is None:
            self.G = self.init_G()
        else:
            self.G = G

        self.A = self.calc_A()

        self.r = self.init_r()

        self.x = self.init_x()

    def init_N(self):
        n = self.n
        N = np.random.normal(0, 1, (n, n))
        np.fill_diagonal(N, 0)
        return N

    def init_H(self):
        alpha = self.alpha
        n = self.n
        h_ = np.random.power(alpha, n)
        h_ = h_ / np.mean(h_)
        H = np.diag(h_)
        return H

    def init_G(self):
        n = self.n
        p = self.p
        G = np.asarray(nx.to_numpy_matrix(nx.gnp_random_graph(n, p, directed=True)))
        np.fill_diagonal(G, 0)
        return G

    def calc_A(self):
        N = self.N
        H = self.H
        G = self.G
        s = self.s
        NH = np.matmul(N, H)
        A = np.multiply(NH, G)
        A = A * s
        np.fill_diagonal(A, -0.5)
        return A

    def init_r(self):
        n = self.n
        r = np.random.uniform(0, 1, n)
        return r

    def init_x(self):
        n = self.n
        time = self.time
        x = np.empty([n, time])
        x[:, 0] = np.random.uniform(0, 1, n)
        return x

    def run(self, n=True):
        A = self.A
        x = self.x
        time = self.time
        r = self.r
        n = self.n

        for i in range(1, time):
            x_dot = np.matmul(np.diag(x[:, i - 1]), r + np.matmul(A, x[:, i - 1]))
            if (n):
                noise = np.random.normal(0, 0.1, n)
            else:
                noise = 0
            x[:, i] = (x[:, i - 1] + x_dot + noise).clip(0)
            # normalize the data
            #x[:, i] = [float(j) / max(x[:, i]) for j in x[:, i]]

        self.x = x
        self.visualize(filename="vis_before_normalize.html")

        self.visualize_G(filename="vis_G.html")

        # normalize the data
        #for i in range(0, time):
        #    x[:, i] = [float(j) / max(x[:, i]) for j in x[:, i]]

        # normalize Z-score
        for i in range(0, time):
            x[:, i] = [(float(j) - np.mean(x[:, i])) / np.std(x[:, i]) for j in x[:, i]]

        self.x = x
        self.visualize(filename="vis_after_normalize.html")

        # Applying Savitzky-Golay filter
        for i in range(0, n):
            x[i, :] = sg.savgol_filter(x[i, :], 5, 2)

        self.x = x
        self.visualize(filename="vis_after_SGfilter.html")


    def get_x(self):
        return self.x

    def visualize_G(self, filename="vis_G.html", G_= None):
        if(G_ is None):
            G = self.G
        else:
            G = G_
        n = self.n

        trace = go.Heatmap(z=G, x=range(0, n), y=range(0, n))
        data = [trace]

        fig = dict(data=data)
        plotly.offline.plot(fig, filename=filename)

    def visualize_A(self, A=-1, filename="vis_A.html"):

        if (type(A) == int):
            A = self.A

        trace = go.Heatmap(z=A)
        data = [trace]

        fig = dict(data=data)
        plotly.offline.plot(fig, filename=filename)

    def visualize_H(self, filename="vis_H.html"):
        H = self.H

        n = self.n

        trace = go.Bar(x=range(0, n), y=H.diagonal())
        data = [trace]

        fig = dict(data=data)
        plotly.offline.plot(fig, filename=filename)

    def visualize(self, x=-1, x_axis_len=-1, filename="vis.html"):

        n = self.n
        if (type(x) == int):
            x = self.x

        if (x_axis_len == -1):
            x_axis_len = self.time

        data = []

        for i in range(0, n):
            data.append(go.Scatter(
                x=range(0, x_axis_len),
                y=x[i, :] / max(1, min(x[i, :])),
                mode='lines',
                name=str(i),
                hoverinfo='name+y',
            ))

        layout = dict(
            legend=dict(
                y=0.5,
                font=dict(
                    size=16
                )
            )
        )

        fig = dict(data=data, layout=layout)
        plotly.offline.plot(fig, filename=filename)

    def visualize_stack(self, x, timesteps):

        ys = x.tolist()
        xs = range(0, timesteps)

        plt.figure(figsize=(100, 20))

        plt.stackplot(xs, *ys)

        plt.legend()
        plt.show()

    def rolling_window(self, l=5, s=2):
        x = self.x
        time = self.time
        # l = length
        # s = step size

        array = []

        # print x

        num = int(math.ceil((self.time - l) / (s * 1.0) + 1))

        for i in range(0, num):
            if (i == num - 1):
                # print time-l, time
                # print x[:, time-l:time]
                array.append(x[:, time - l:time])
            else:
                # print i*s, ":", i*s+l
                # print x[:, i*s:i*s+l]
                array.append(x[:, i * s:i * s + l])

        self.array = array
        return array, num