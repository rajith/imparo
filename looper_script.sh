#!/bin/bash

for i in `seq 1 3`;
do
    for j in 10000 5000 1000 500 100 50 10 5;
    do
        qsub -v nettype=$i,timesteps=$j,numtests=100,name=$i.$j train_test.sh
    done
done