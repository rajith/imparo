import keras
import time
import numpy as np
import math

from keras.layers import Dense, LSTM, SimpleRNN, GRU
from keras.models import Sequential, load_model

from dataset import DataSet

def create_model_1(timesteps):
    '''
    Create the neural network 1
    Has 1 layer of LSTM
    :param timesteps:
    :return:
    '''
    print("Creating model1")
    model = Sequential()
    model.add(LSTM(256, input_shape=(timesteps, 2)))
    model.add(Dense(1))
    model.add(Dense(1))
    model.compile(loss = 'binary_crossentropy',
                  optimizer = 'rmsprop',
                  metrics = ['accuracy'])
    return model

def create_model_2(timesteps):
    '''
    Create the neural network 2
    Has 1 layer of SimpleRNN
    :param timesteps:
    :return:
    '''
    model = Sequential()
    model.add(SimpleRNN(32, input_shape=(timesteps, 2)))
    model.add(Dense(1))
    model.compile(loss = 'binary_crossentropy',
                  optimizer = 'rmsprop',
                  metrics = ['accuracy'])
    return model

def create_model_3(timesteps):
    '''
    Create the neural network 3
    Has 1 layer of Gated Recurrent Units
    :param timesteps:
    :return:
    '''
    model = Sequential()
    model.add(GRU(256, input_shape=(timesteps, 2)))
    model.add(Dense(1))
    model.add(Dense(1))
    model.compile(loss = 'binary_crossentropy',
                  optimizer = 'rmsprop',
                  metrics = ['accuracy'])
    return model

def train_model(model, file_name, timesteps):

    print("Training model: "+file_name)

    ds = DataSet(n=100, alpha=1, s=1, time=timesteps, p=0.01)
    ds.run()

    x_train, y_train = transform_data_for_testing(ds)

    hist = model.fit(x_train, y_train, batch_size=128, nb_epoch=10,
                     validation_split=0.1, verbose=0)

    model.save(file_name)
    print("FInished training model, and saved to: "+file_name)


def load_model_from_file(path):
    '''
    Load a neural network model from a file location
    :param path: location of the saved model
    :return: the model
    '''
    return load_model(path)

def transform_data_for_testing(dataset):
    '''
    Transforms a dataset for testing / or training the Neural Network (G)
    :param dataset: the dataset to be used
    :return: x_test, y_test vectors
    '''
    #print dataset.n
    x_test = np.empty([dataset.n * (dataset.n-1), dataset.time, 2])
    y_test = np.empty([dataset.n * (dataset.n-1)])

    counter = 0
    for i in range(dataset.n):
        for j in range(dataset.n):
            if i == j:
                continue
            else:
                x_test[counter, :, 0] = dataset.x[i]
                x_test[counter, :, 1] = dataset.x[j]
                y_test[counter] = dataset.G[i, j]
                counter += 1

    return x_test, y_test

def transform_data_for_predicting(dataset):
    '''
    Transform a dataset for predicting G
    :param dataset: the dataset to be used
    :return: x_predict vector
    '''

    x_predict = np.empty([dataset.n * (dataset.n-1), dataset.time, 2])

    counter = 0
    for i in range(dataset.n):
        for j in range(dataset.n):
            if i == j:
                continue
            else:
                x_predict[counter, :, 0] = dataset.x[i]
                x_predict[counter, :, 1] = dataset.x[j]
                counter += 1

    return x_predict

def transform_data_from_prediction(y_predict):
    '''
    Transform predicted G values into a matrix nxn
    :param y_predict: the predicted connectedness values
    :return: G_cross - predicted G in matrix form
    '''

    size = y_predict.size
    n = (1 + math.sqrt(1 + 4*size))/2
    print("transform_data_from_prediction: n=", n)
    n = int(n)
    print("transform_data_from_prediction: n=", n)
    G_cross = np.empty([n, n])

    counter = 0
    for i in range(n):
        for j in range(n):
            if i == j:
                G_cross[i, i] = 0
            else:
                G_cross[i, j] = y_predict[counter]
                counter += 1

    return G_cross

def run_tests(num_test, model, timesteps):
    '''
    Runs num_test number of tests on the model
    :param num_test: number of tests to be run
    :param model: the model to be run
    :return: None. Output is printed to the screen
    '''

    print("Starting tests")

    sum = 0.0

    ds1 = DataSet(n=10, alpha=9, s=1, time=timesteps, p=0.5)

    G_global = ds1.G;

    for i in range(num_test):

        ds = DataSet(n=10, alpha=9, s=1, time=timesteps, p=0.5, G=G_global)
        ds.run()
        x_test, y_test = transform_data_for_testing(ds)

        score, acc = model.evaluate(x_test, y_test, verbose=0, batch_size=16)
        #print(acc)
        sum += acc

    print("Finished tests")
    print("Average accuracy: "+str(sum/num_test))

def predict_G(dataset, model):
    '''
    Predict the connectedness matrix (G) of a given dataset by using the abundance profiles (x)
    :param timesteps: number of timesteps
    :param dataset: the dataset on which to predict
    :param model: use the model for prediction
    :return: return G matrix - n x n
    '''

    print("Predicting G")

    ds = dataset

    x_predict = transform_data_for_predicting(ds)
    y_predict = model.predict_classes(x_predict, verbose=0, batch_size=16)

    print("y Predict:", y_predict)

    G_cross = transform_data_from_prediction(y_predict)

    return G_cross

def compare_G(G, G_cross):
    G_r = G.shape[0] # no of rows in G
    G_c = G.shape[1] # no of columns in G

    G_cross_r = G_cross.shape[0] # ditto G_Cross
    G_cross_c = G_cross.shape[1]

    if(G_r != G_cross_r and G_c != G_cross_c):
        print("Dimensions of G and G_Cross are different! (",G_r,", ",G_c,"), (",G_cross_r,", ",G_cross_c,")")
        return 0
    if(G_r != G_c or G_cross_r != G_cross_c):
        print("G and/or G_cross not sqaure! (",G_r,", ",G_c,"), (",G_cross_r,", ",G_cross_c,")")
        return 0

    counter = 0

    for i in range(G_r):
        for j in range(G_c):
            if(G[i, j] == G_cross[i, j]):
                counter += 1

    print("Counter", counter)

    return counter*100.0 / (G_r * G_c)


class TimeHistory(keras.callbacks.Callback):
    '''
    Callbacks for timing the neural network epochs
    '''
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        print time.time() - self.epoch_time_start
        self.times.append(time.time() - self.epoch_time_start)